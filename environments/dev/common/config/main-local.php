<?php
return [
    'components' => [
        'db' => [
	        'class' => 'yii\db\Connection',
	        'dsn' => 'mysql:host=mysql;dbname=cmz',
	        'username' => 'root',
	        'password' => 'EWotjUyXBTPnT6dbGamb2Chi',
	        'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
    'aliases'    => [
	    '@files'     => '/var/www/cmz/files',
	    '@filesView' => '//files.cmz.local/'
    ]
];
