<?php
namespace cabinet\controllers;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class BaseController extends Controller
{
	public function can($role)
	{
		if (!\Yii::$app->user->can($role)) {
			throw new ForbiddenHttpException('Доступ запрещён');
		}
		return true;
	}
	
	public function init()
	{
		parent::init();
		
		if (\Yii::$app->user->isGuest) {
			$this->redirect(['/user/login']);
			\Yii::$app->end();
		} elseif (\Yii::$app->user->identity->isBlocked) {
			\Yii::$app->getUser()->logout();
			\Yii::$app->getSession()->setFlash('error', 'Ваш аккаунт заблокирован');
			return $this->goHome();
		}
	}
	
}