<?php
$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);

return [
	'id'                  => 'app-cabinet',
	'basePath'            => dirname(__DIR__),
	'controllerNamespace' => 'cabinet\controllers',
	'bootstrap'           => ['log'],
	'modules'             => [
		'user' => [
			'class'              => \dektrium\user\Module::class,
			'enableRegistration' => false,
			'enableConfirmation' => false,
			'admins'             => ['nadia'],
			'controllerMap'      => [
				'security' => \cabinet\controllers\SecurityController::class
			],
			'modelMap'           => [
				'User' => \common\models\User::className()
			],
		],
		'rbac' => [
			'class' => 'dektrium\rbac\RbacWebModule',
		],
	],
	'components'          => [
		'request'      => [
			'csrfParam' => '_csrf-cabinet',
		],
		'user'         => [
			'identityClass'   => 'common\models\User',
			'enableAutoLogin' => true,
			'identityCookie'  => ['name' => '_identity-cabinet', 'httpOnly' => true],
		],
		'session'      => [
			// this is the name of the session cookie used for login on the cabinet
			'name' => 'advanced-cabinet',
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'view'         => [
			'theme' => [
				'pathMap' => [
					'@dektrium/user/views' => '@cabinet/views/user'
				],
			],
		],
		'urlManager'   => [
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
			'rules'           => [
				'<controller>/<action>'                  => '<controller>/<action>',
				'<controller:\w+>/<id:\d+>'              => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
			]
		]
	],
	'params'              => $params,
];
