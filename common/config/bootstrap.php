<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@registration', dirname(dirname(__DIR__)) . '/registration');
Yii::setAlias('@agent', dirname(dirname(__DIR__)) . '/agent');
Yii::setAlias('@cabinet', dirname(dirname(__DIR__)) . '/cabinet');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@admin', dirname(dirname(__DIR__)) . '/admin');
