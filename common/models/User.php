<?php
namespace common\models;

use dektrium\user\models\User as BaseUser;
use Yii;

/**
 * User model
 * @property string $phone
 * @property string $city
 * @property string $firstName
 * @property string $lastName
 */
class User extends BaseUser
{
	const ROLE_ADMIN = 'admin';
	const ROLE_AGENT = 'agent';
	const ROLE_DEVELOPER = 'developer';
	
	public function scenarios()
	{
		$scenarios = parent::scenarios();
		// add field to scenarios
		$scenarios['create'][] = 'phone';
		$scenarios['update'][] = 'phone';
		$scenarios['register'][] = 'phone';
		
		$scenarios['create'][] = 'city';
		$scenarios['update'][] = 'city';
		$scenarios['register'][] = 'city';
		
		$scenarios['create'][] = 'firstName';
		$scenarios['update'][] = 'firstName';
		$scenarios['register'][] = 'firstName';
		
		$scenarios['create'][] = 'lastName';
		$scenarios['update'][] = 'lastName';
		$scenarios['register'][] = 'lastName';
		
		return $scenarios;
	}
	
	public function rules()
	{
		$rules = parent::rules();
		
		$rules['cityType'] = ['city', 'string'];
		$rules['firstNameType'] = ['firstName', 'string'];
		$rules['lastNameType'] = ['lastName', 'string'];
		$rules['phoneType'] = ['phone', 'string'];
		
		return $rules;
	}
	
	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		
		return $labels;
	}
}