<?php

namespace common\models;

use Imagine\Gd\Imagine;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "products".
 *
 * @property int         $id
 * @property string      $title
 * @property string|null $description
 * @property string|null $imgPath
 * @property float|null  $price
 * @property int         $created_at
 * @property int         $updated_at
 */
class Product extends \yii\db\ActiveRecord
{
	public $photoFile;
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'products';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['description'], 'string'],
			[['price'], 'number'],
			[['created_at', 'updated_at'], 'integer'],
			[['title', 'imgPath'], 'string', 'max' => 255],
			['photoFile', 'file', 'extensions' => 'jpg, jpeg, png', 'mimeTypes' => 'image/jpeg, image/png',
			                      'maxFiles'   => 1, 'maxSize' => 5000000,
			                      'tooBig'     => 'Размер файла не должен превышать 5МБ'],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id'          => 'ID',
			'title'       => 'Название',
			'description' => 'Описание',
			'imgPath'     => 'Картинка',
			'photoFile'   => 'Картинка',
			'price'       => 'Цена',
			'created_at'  => 'Создан',
			'updated_at'  => 'Обновлен',
		];
	}
	
	public function beforeSave($insert)
	{
		if ($insert) {
			$this->created_at = time();
		}
		$this->updated_at = time();
		
		$file = UploadedFile::getInstance($this, 'photoFile');
		if ($file) {
			if ($this->imgPath) {
				FileHelper::deleteFile($this->imgPath);
			}
			$dir = \Yii::getAlias('@files') . '/' . 'products';
			if (!file_exists($dir)) {
				mkdir($dir);
			}
			$title = uniqid() . '.' . $file->extension;
			$folder = $dir . '/' . $title;
			if ($file->saveAs($folder)) {
				$this->imgPath = 'products/' . $title;
				$imagine = new Imagine();
				$resizeImage = $imagine->open(\Yii::getAlias('@files') . '/' . $this->imgPath);//Открываем временное изображение, существующее на сервере только до следующего request.
				$quality = round(2000000 / $file->size * 100); //Высчитваем на сколько процентов надо снизить качество изображения, чтобы быть в пределах 2 Мб
				$resizeImage->save(\Yii::getAlias('@files') . '/' . $this->imgPath, ['quality' => $quality]);// Сохраняем с учетом выситанного ккачества.
			}
		}
		
		return parent::beforeSave($insert);
	}
}
