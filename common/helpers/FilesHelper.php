<?php
namespace common\helpers;

use Yii;

class FilesHelper
{
	public static function deleteFile($folder = null)
	{
		if (!$folder || $folder == '') {
			return true;
		}
		$filePath = Yii::getAlias('@files') . '/' . $folder;
		if (file_exists($filePath)) {
			unlink($filePath);
			
			return true;
		}
		
		return false;
	}
}