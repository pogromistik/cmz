<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Products}}`.
 */
class m200211_170624_create_Products_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%products}}', [
			'id'          => $this->primaryKey(),
			'title'       => $this->string()->notNull(),
			'description' => $this->text(),
			'imgPath'     => $this->string(),
			'price'       => $this->float(),
			'created_at'  => $this->integer()->notNull(),
			'updated_at'  => $this->integer()->notNull(),
		]);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%products}}');
	}
}
