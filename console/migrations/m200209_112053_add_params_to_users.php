<?php

use registration\models\User;
use yii\db\Migration;

/**
 * Class m200209_112053_add_params_to_users
 */
class m200209_112053_add_params_to_users extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->addColumn(User::tableName(), 'phone', $this->string());
		$this->addColumn(User::tableName(), 'city', $this->string());
		$this->addColumn(User::tableName(), 'firstName', $this->string());
		$this->addColumn(User::tableName(), 'lastName', $this->string());
		$this->addColumn(User::tableName(), 'subDomain', $this->integer());
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn(User::tableName(), 'phone');
		$this->dropColumn(User::tableName(), 'city');
		$this->dropColumn(User::tableName(), 'firstName');
		$this->dropColumn(User::tableName(), 'lastName');
		$this->dropColumn(User::tableName(), 'subDomain');
	}
}
