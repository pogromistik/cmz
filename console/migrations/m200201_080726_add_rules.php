<?php

use yii\db\Migration;

/**
 * Class m200201_080726_add_rules
 */
class m200201_080726_add_rules extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->insert('auth_item', [
			'name'        => \common\models\User::ROLE_DEVELOPER,
			'type'        => 1,
			'description' => 'Разработчик',
			'created_at'  => time(),
			'updated_at'  => time()
		]);
		$this->insert('auth_item', [
			'name'        => \common\models\User::ROLE_ADMIN,
			'type'        => 1,
			'description' => 'Администратор',
			'created_at'  => time(),
			'updated_at'  => time()
		]);
		$this->insert('auth_item', [
			'name'        => \common\models\User::ROLE_AGENT,
			'type'        => 1,
			'description' => 'Агент',
			'created_at'  => time(),
			'updated_at'  => time()
		]);
		$this->insert('auth_item_child', ['parent' => \common\models\User::ROLE_DEVELOPER, 'child' => \common\models\User::ROLE_ADMIN]);
		$this->insert('auth_item_child', ['parent' => \common\models\User::ROLE_ADMIN, 'child' => \common\models\User::ROLE_AGENT]);
	}
	public function safeDown()
	{
		$this->delete('auth_item', ['name' => \common\models\User::ROLE_AGENT]);
		$this->delete('auth_item', ['name' => \common\models\User::ROLE_ADMIN]);
		$this->delete('auth_item', ['name' => \common\models\User::ROLE_DEVELOPER]);
	}
}
