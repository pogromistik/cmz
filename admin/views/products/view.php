<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить этот товар? Действие необратимо',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:html',
            [
				'attribute' => 'imgPath',
				'format'    => 'raw',
				'value'     => $model->imgPath ? Html::img(\Yii::getAlias('@filesView') . '/' . $model->imgPath) : ''
			],
            'price',
	        [
		        'attribute' => 'created_at',
		        'format'    => 'raw',
		        'value'     => date("d.m.Y, H:i", $model->created_at)
	        ],
	        [
		        'attribute' => 'updated_at',
		        'format'    => 'raw',
		        'value'     => date("d.m.Y, H:i", $model->updated_at)
	        ],
        ],
    ]) ?>

</div>
