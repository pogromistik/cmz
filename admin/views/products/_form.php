<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">
	
	<?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'description')->widget(CKEditor::className(), [
		'preset'        => 'full',
		'clientOptions' => [
			'filebrowserImageUploadUrl' => '/products/upload',
			'allowedContent'            => true,
			'height'                    => 150
		],
	]) ?>
	
    <?php if ($model->imgPath) { ?>
        <?= Html::img(\Yii::getAlias('@filesView') . '/' . $model->imgPath) ?>
    <?php } ?>
	<?= $form->field($model, 'photoFile')->fileInput() ?>
	
	<?= $form->field($model, 'price')->textInput() ?>

    <div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
	
	<?php ActiveForm::end(); ?>

</div>
