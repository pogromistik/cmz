<?php
/**
 * @var string $assetDir
 */

use hail812\adminlte3\widgets\Menu;
use yii\helpers\Url;

?>
<aside class="main-sidebar sidebar-dark-danger elevation-4">
    <!-- Brand Logo -->
    <a href="<?= Url::home() ?>" class="brand-link">
        <img src="<?= $assetDir ?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Вакансии</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/img/avatar.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="<?= Url::to(['/users/update', 'id' => \Yii::$app->user->id]) ?>" class="d-block"><?= \Yii::$app->user->identity->username ?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">

			<?php
			echo Menu::widget([
				'items' => [
					['label' => 'Вакансии', 'icon' => 'file', 'url' => ['/vacancies']],
					['label' => 'Отклики', 'icon' => 'users', 'url' => ['/resume']],
					['label' => 'Тексты писем', 'icon' => 'envelope-open-text', 'url' => ['/mails']]
				],
			]);
			?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>