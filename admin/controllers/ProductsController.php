<?php

namespace admin\controllers;

use common\helpers\FilesHelper;
use Imagine\Gd\Imagine;
use Yii;
use common\models\Product;
use common\models\search\ProductSearch;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Product model.
 */
class ProductsController extends BaseController
{
	/**
	 * Lists all Product models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ProductSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	
	/**
	 * Displays a single Product model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}
	
	/**
	 * Creates a new Product model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Product();
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		}
		
		return $this->render('create', [
			'model' => $model,
		]);
	}
	
	/**
	 * Updates an existing Product model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		}
		
		return $this->render('update', [
			'model' => $model,
		]);
	}
	
	/**
	 * Deletes an existing Product model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		if ($model->imgPath) {
			FilesHelper::deleteFile($model->imgPath);
		}
		$model->delete();
		
		return $this->redirect(['index']);
	}
	
	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Product::findOne($id)) !== null) {
			return $model;
		}
		
		throw new NotFoundHttpException('The requested page does not exist.');
	}
	
	public function actionUpload($CKEditorFuncNum)
	{
		$file = UploadedFile::getInstanceByName('upload');
		if ($file)
		{
			$dir = \Yii::getAlias('@files') . '/ckEditor';
			if (!file_exists($dir)) {
				mkdir($dir);
			}
			
			$fileName = round(microtime(true) * 1000) . '.' . $file->extension;
			
			if ($file->saveAs($dir . '/' . $fileName)) {
				$path = $dir . '/' . $fileName;
				$imagine = new Imagine();
				$resizeImage = $imagine->open($path);
				$quality = round(2000000 / $file->size * 100);
				$resizeImage->save($path, ['quality' => $quality]);
				
				return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $CKEditorFuncNum . '", "'
					. $this->getUrlForEditor($fileName) . '", "");</script>';
			}
			else {
				return "Возникла ошибка при загрузке файла\n";
			}
		}
		else
			return "Файл не загружен\n";
	}
	
	public function getUrlForEditor($fileName)
	{
		return \Yii::getAlias('@filesView') . '/ckEditor/' . $fileName;
	}
}
