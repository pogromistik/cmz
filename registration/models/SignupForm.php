<?php

namespace registration\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $phone
 * @property string $city
 */
class SignupForm extends \yii\db\ActiveRecord
{
	public $firstName;
	public $lastName;
	public $email;
	public $phone;
	public $city;
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['email', 'phone', 'firstName', 'lastName', 'city'], 'string', 'max' => 255]
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'email'     => 'Email',
			'phone'     => 'Телефон',
			'firstName' => 'Имя',
			'lastName'  => 'Фамилия',
			'city'      => 'Город',
		];
	}
}
