<?php

namespace registration\assets;

use yii\web\AssetBundle;

/**
 * Main registration application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.min.css',
        'css/flaticon-set.css',
        'css/magnific-popup.css',
        'css/owl.carousel.min.css',
        'css/owl.theme.default.min.css',
        'css/animate.css',
        'css/bootsnav.css',
        'css/style.css',
        'css/responsive.css',
    ];
    public $js = [
    	'js/equal-height.min.js',
    	'js/jquery.appear.js',
    	'js/jquery.easing.min.js',
    	'js/jquery.magnific-popup.min.js',
    	'js/modernizr.custom.13711.js',
    	'js/owl.carousel.min.js',
    	'js/wow.min.js',
    	'js/count-to.js',
    	'js/bootsnav.js',
    	'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	    'yii\bootstrap\BootstrapPluginAsset'
    ];
}
