<?php

namespace registration\controllers;

use registration\models\User;
use dektrium\rbac\models\Assignment;
use Yii;
use yii\web\Controller;
use registration\models\SignupForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			]
		];
	}
	
	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}
	
	/**
	 * @return bool|string
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 */
	public function actionRegistration()
	{
		$model = new SignupForm();
		$model->load(\Yii::$app->request->post());
		$model->firstName = trim(strip_tags($model->firstName));
		if (!$model->firstName) {
			return 'Укажите имя';
		}
		$model->lastName = trim(strip_tags($model->lastName));
		if (!$model->lastName) {
			return 'Укажите фамилию';
		}
		$model->city = trim(strip_tags($model->city));
		if (!$model->city) {
			return 'Укажите город';
		}
		$model->phone = trim(strip_tags($model->phone));
		if (!$model->phone) {
			return 'Укажите телефон';
		}
		$model->email = trim(strip_tags($model->email));
		if (!$model->email) {
			return 'Укажите email';
		}
		if (User::findOne(['email' => $model->email])) {
			return 'Указанный email занят';
		}
		
		$user = new User();
		$user->firstName = $model->firstName;
		$user->lastName = $model->lastName;
		$user->email = $model->email;
		$user->phone = $model->phone;
		$user->city = $model->city;
		$user->created_at = time();
		$user->confirmed_at = time();
		$user->updated_at = time();
		$user->flags = 0;
		$user->username = $model->email;
		$password = User::generatePassword();
		$user->setPassword($password);
		$user->generateAuthKey();
		
		if ($user->save()) {
			$user->subDomain = $user->id + 5031;
			$user->save(false);
			
			\Yii::$app->db->createCommand()->insert("auth_assignment", [
				'item_name'  => \common\models\User::ROLE_AGENT,
				'created_at' => time(),
				'user_id'    => $user->id
			])->execute();
			
			/*\Yii::$app->mailer->compose('login', ['user' => $user, 'password' => $password])
				->setTo($user->email)
				->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->params['companyName']])
				->setSubject('Регистрация')
				->send();*/
			
			return true;
		}
		
		return true;
	}
}
