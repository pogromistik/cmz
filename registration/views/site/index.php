<?php

/* @var $this yii\web\View */

$this->title = 'Партнёрская программа';
$model = new \registration\models\SignupForm();
?>
<!-- Start Banner
   ============================================= -->
<div class="banner-area text-small content-double shadow dark text-light bg-fixed"
     style="background-image: url(/img/main.jpg);">
    <div class="box-table">
        <div class="box-cell">
            <div class="container">
                <div class="row">
                    <div class="double-items">
                        <!-- Banner Left Content -->
                        <div class="info col-md-6">
                            <div class="content">
                                <h1>Зарегистрируйтесь в партнёрской программе</h1>
                                <p>
                                    Прими участие в партнёрской программе ООО «ЧМЗ» - получи вознаграждение за каждую
                                    совершенную сделку.
                                </p>
                                <a href="#about" class="smooth-menu btn circle btn-light effect btn-md">Узнать
                                    подробности</a>
                            </div>
                        </div>
                        <!-- End Banner Left Content -->

                        <!-- Start Banner Form -->
                        <div class="form col-md-5 shadow-light col-md-offset-1">
                            <div class="form-info">
                                <h2><span>Регистрация</span> в партнёрской программе.</h2>
								<?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['class' => 'registrationForm']]) ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
										<?= $form->field($model, 'firstName')->textInput(['placeholder' => 'Имя*'])->label(false) ?>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
										<?= $form->field($model, 'lastName')->textInput(['placeholder' => 'Фамилия*'])->label(false) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
										<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email*'])->label(false) ?>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
										<?= $form->field($model, 'phone')
											->widget(\yii\widgets\MaskedInput::className(), [
												'mask'    => '+7 (999) 999 9999',
												'options' => ['placeholder' => 'Телефон*']
											])->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group">
											<?= $form->field($model, 'city')->textInput(['placeholder' => 'Город*'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="error"></div>
										<?= \yii\helpers\Html::submitButton('Зарегистрироваться') ?>
                                        <div class="success"></div>
                                    </div>
                                </div>
								<?php $form->end() ?>
                            </div>
                        </div>
                        <!-- End Banner Form -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Banner -->

<!-- Start About
============================================= -->
<div id="about" class="work-list-area default-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="site-heading text-center">
                    <h2>Как это <span>работает</span></h2>
                    <h4>Схема работы в партнёрской программе</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 overview-items solid-items">
                <!-- Start Tab Content -->
                <div class="row">

                    <!-- Start Single Item -->
                    <div class="single-item">
                        <div class="col-md-6 thumb">
                            <img src="assets/img/illustrations/3.svg" alt="Thumb">
                        </div>
                        <div class="col-md-6 info">
                            <ul>
                                <li>
                                    <h4>Зарегистрируйтесь на этом сайте</h4>
                                    После регистрации вам на почту придёт письмо с данными для входа в личный кабинет и
                                    персональный сайт для рекламы нашей продукции
                                </li>
                                <li>
                                    <h4>Приведите клиента</h4>
                                    Оставьте нам номер телефона человека, интересующегося нашей продукцией
                                </li>
                                <li>
                                    <h4>Отслеживайте работу с вашим клиентом в личном кабинете</h4>
                                    В личном кабинете вы сможете видеть всю историю работы менеджера с вашим клиентом, а
                                    так же прослушивать запись разговоров
                                </li>
                                <li>
                                    <h4>Получите вознаграждение</h4>
                                    Вознаграждение выплачивается за каждого клиента, совершившего покупку
                                </li>
                            </ul>
                            <p>
                                От вас не требуется никаких вложений, а так же не обязательно разбираться в нашей
                                продукции. Всё, что требуется - передать клиента нашим менеджерам.
                            </p>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
                <!-- End Tab Content -->
            </div>
        </div>
    </div>
</div>
<!-- End About -->

<!-- Start We Offer
============================================= -->
<div id="company" class="we-offer-area text-center bg-gray default-padding bottom-less">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="site-heading text-center">
                    <h2>О нас</h2>
                    <h4></h4>
                </div>
            </div>
        </div>
        <div class="row text-left">
            <p>
                Компания ООО «ЧМЗ» была организована в 2013 году. Мы занимаемся производством и продажей тренажеров,
                турников и других товаров для занятий
                спортом, а также выпускаем аксессуары к ним.
            </p>
            <p>
                Конструкторский отдел и производственные мощности позволяют выпускать как серийную продукцию, так и
                производить товары по индивидуальным заказам.
            </p>
            <p>
            <ul>
                Мы поставляем товары для:
                <li>— тренажерных залов</li>
                <li>— реабилитационных и медицинских центров</li>
                <li>— покупателей, желающих установить тренажер у себя дома</li>
            </ul>
            </p>
            <p>
                Главным преимуществом мы считаем научно-производственный потенциал группы наших компаний, который дает
                возможность совершенствовать наши товары с точки зрения конструкторских решений. Мы постоянно
                отслеживаем
                появление новинок на рынке комплектующих, которые поднимают уровень качества изделий и уровень
                комфортности
                для спортсменов, а также стремимся снижать себестоимость, делая наши продукты доступными.
            </p>
        </div>
    </div>
</div>
<!-- End We Offer -->

<!-- Start Contact Area
============================================= -->
<div id="contact" class="contact-us-area default-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="site-heading text-center">
                    <h2>Контакты</h2>
                    <h4></h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 contact-form">
                <h2>Регистрация в партнёрской программе</h2>
	            <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['class' => 'registrationForm']]) ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
			            <?= $form->field($model, 'firstName')->textInput(['placeholder' => 'Имя*'])->label(false) ?>
                    </div>
                    <div class="col-md-6 col-sm-12">
			            <?= $form->field($model, 'lastName')->textInput(['placeholder' => 'Фамилия*'])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
			            <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email*'])->label(false) ?>
                    </div>
                    <div class="col-md-6 col-sm-12">
			            <?= $form->field($model, 'phone')
				            ->widget(\yii\widgets\MaskedInput::className(), [
					            'mask'    => '+7 (999) 999 9999',
					            'options' => ['placeholder' => 'Телефон*']
				            ])->label(false) ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group">
				            <?= $form->field($model, 'city')->textInput(['placeholder' => 'Город*'])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="error"></div>
			            <?= \yii\helpers\Html::submitButton('Зарегистрироваться') ?>
                        <div class="success"></div>
                    </div>
                </div>
                <?php $form->end() ?>
            </div>
            <div class="col-md-4 address">
                <div class="address-items">
                    <ul class="info">
                        <li>
                            <h4>Адрес</h4>
                            <div class="icon"><i class="fas fa-map-marked-alt"></i></div>
                            <span>г. Челябинск,<br>ул. Монтажников, 8</span>
                        </li>
                        <li>
                            <h4>Телефон</h4>
                            <div class="icon"><i class="fas fa-phone"></i></div>
                            <a href="tel:78003021281">+7 (800) 30-212-81</a>
                        </li>
                        <li>
                            <h4>Email</h4>
                            <div class="icon"><i class="fas fa-envelope-open"></i></div>
                            <a href="mailto:sale@sport999.ru">sale@sport999.ru</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Contact -->

<!-- Start Google Maps
============================================= -->
<div class="maps-area">
    <div class="container-full">
        <div class="row">
            <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14767.262289338461!2d70.79414485000001!3d22.284975!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1424308883981"></iframe>
            </div>
        </div>
    </div>
</div>
<!-- End Google Maps -->