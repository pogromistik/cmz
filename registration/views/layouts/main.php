<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use registration\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Preloader Start -->
<div class="se-pre-con"></div>
<!-- Preloader Ends -->

<!-- Header
============================================= -->
<header id="home">

    <!-- Start Navigation -->
    <nav class="navbar navbar-default navbar-sticky bootsnav">

        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="/img/logo.png" class="logo logo-display" alt="Logo">
                </a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="#" data-out="#">
                    <li>
                        <a class="smooth-menu" href="#about">Подробнее</a>
                    </li>
                    <li>
                        <a class="smooth-menu" href="#company">О нас</a>
                    </li>
                    <li>
                        <a class="smooth-menu" href="#contact">Контакты</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>

    </nav>
    <!-- End Navigation -->

</header>
<!-- End Header -->

<?= $content ?>

<!-- Start Footer
    ============================================= -->
<footer class="default-padding-top bg-light">
    <div class="container">
        <div class="row">
            <div class="f-items">
                <div class="col-md-5 col-sm-6 equal-height item">
                    <div class="f-item about">
                        <img src="/img/logo.png" alt="Logo">
                        <p>
                            Занимаемся производством и продажей тренажеров, турников и других товаров для занятий спортом, а также выпускаем
                            аксессуары к ним с 2013 года.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 equal-height item">
                    <div class="f-item link">
                        <h4>Меню</h4>
                        <ul>
                            <li>
                                <a class="smooth-menu" href="#about">Подробнее</a>
                            </li>
                            <li>
                                <a class="smooth-menu" href="#company">О нас</a>
                            </li>
                            <li>
                                <a class="smooth-menu" href="#contact">Контакты</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 equal-height item">
                    <div class="f-item twitter-widget">
                        <h4>Контактная информация</h4>
                        <p>
                            Свяжитесь с вами, если у вас остались вопросы
                        </p>
                        <div class="address">
                            <ul>
                                <li>
                                    <div class="icon">
                                        <i class="fas fa-home"></i>
                                    </div>
                                    <div class="info">
                                        <h5>Сайт:</h5>
                                        <a href="https://чмз.онлайн" target="_blank">чмз.онлайн</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="fas fa-envelope"></i>
                                    </div>
                                    <div class="info">
                                        <h5>Email:</h5>
                                        <a href="mailto:sale@sport999.ru">sale@sport999.ru</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="fas fa-phone"></i>
                                    </div>
                                    <div class="info">
                                        <h5>Phone:</h5>
                                        <a href="tel:78003021281">+7 (800) 30-212-81</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <br>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Bottom -->
</footer>
<!-- End Footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
